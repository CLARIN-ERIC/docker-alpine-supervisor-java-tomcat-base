docker run -ti --rm -v "$(pwd)/../image/usr/bin:/data" --workdir=/data alpine sh -c '\
echo "Installing bash" && \
apk add --no-cache bash && \
touch test.properties && \
./updateKeyValue "test" "def" "test.properties" && \
cat test.properties && \
./updateKeyValue "test" "def" "test.properties" && \
cat test.properties && \
echo "test=abc" > test.properties && \
./updateKeyValue "test" "def" "test.properties" && \
cat test.properties && \
./updateKeyValue "test" "def" "test.properties" && \
cat test.properties && \
rm test.properties
'