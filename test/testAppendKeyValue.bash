#!/usr/bin/env bash

docker run -ti --rm -v "$(pwd)/../image/usr/bin:/data" --workdir=/data alpine sh -c '\
echo "Installing bash" && \
apk add --no-cache bash && \
echo "test=abc" > test.properties && \
./appendKeyValue "test" "def," "test.properties" && \
cat test.properties && \
./appendKeyValue "test" "def," "test.properties" && \
cat test.properties
'