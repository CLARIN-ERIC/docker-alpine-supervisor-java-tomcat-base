FROM registry.gitlab.com/clarin-eric/docker-alpine-supervisor-java-base:openjdk21_jre-1.0.5 as java-base

ENV TOMCAT_MAJOR=9
ENV TOMCAT_VERSION=9.0.93
ENV CATALINA_BASE /srv/tomcat

FROM java-base as build

# Distribution hash from 
# https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz.sha512
ARG TOMCAT_SHA512="3069924eb7041ccc0f2aeceb7d8626793a1a073a5b739a840d7974a18ebeb26cc3374cc5f4a3ffc74d3b019c0cb33e3d1fe96296e6663ac75a73c1171811726d"

ARG APACHE_REPO="https://dlcdn.apache.org/tomcat"
ARG GNUPG_VERSION=2.4.4-r0

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

COPY tomcat-keys.asc /tmp/tomcat-keys.asc

RUN apk add --no-cache \
            "gnupg=$GNUPG_VERSION" \
 && wget -q -O /tmp/tomcat.tar.gz "${APACHE_REPO}/tomcat-${TOMCAT_MAJOR}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz" \
 && wget -q -O /tmp/tomcat.tar.gz.asc "${APACHE_REPO}/tomcat-${TOMCAT_MAJOR}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz.asc" \
 && gpg --import /tmp/tomcat-keys.asc && gpg --verify '/tmp/tomcat.tar.gz.asc' '/tmp/tomcat.tar.gz' \
 && echo "${TOMCAT_SHA512}"' */tmp/tomcat.tar.gz' | sha512sum -c - \
 && mkdir -p "${CATALINA_BASE}" \
 && tar -C "${CATALINA_BASE}" -xvf '/tmp/tomcat.tar.gz' --strip-components=1 \
 && rm -rf -- "${CATALINA_BASE}/webapps/docs/" \
 && rm -rf -- "${CATALINA_BASE}/webapps/examples"

### Package stage

FROM java-base

ARG PCRE_TOOLS_VERSION=8.45-r3

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

ENV CATALINA_HOME ${CATALINA_BASE}
ENV CATALINA_PID /var/run/tomcat8.pid

ENV HEALTHCHECK_TEST_URL_HTTP="http://localhost:8080"
ENV HEALTHCHECK_TEST_URL_HTTPS="https://localhost:8443"

COPY --from=build ${CATALINA_BASE} ${CATALINA_BASE}

RUN apk add --no-cache \
        "pcre-tools=$PCRE_TOOLS_VERSION" \
 && mkdir -p -- \
        "${CATALINA_BASE}/temp" \
        "${CATALINA_BASE}/common/classes" \
        "${CATALINA_BASE}/server/classes" \
        "${CATALINA_BASE}/shared/classes" \
        /cert

COPY tomcat/server.xml ${CATALINA_BASE}/conf/server.xml
COPY tomcat/logging.properties ${CATALINA_BASE}/conf/logging.properties
COPY tomcat/tomcat-users.xml ${CATALINA_BASE}/conf/tomcat-users.xml

COPY supervisor/tomcat.conf /etc/supervisor/conf.d/tomcat.conf
COPY init.sh /init/tomcat_init.sh
COPY fluentd/tomcat.conf /etc/fluentd/conf.d/tomcat.conf

COPY tomcat-healthcheck.bash /etc/healthcheck.d/tomcat-healthcheck.bash

COPY logrotate/tomcat.conf /etc/logrotate.clarin/tomcat.conf

COPY usr/bin /usr/bin

RUN chmod u+x /init/tomcat_init.sh \
 && chmod 0644 /etc/logrotate.clarin/tomcat.conf \
 && addgroup tomcat \
 && adduser -D -G tomcat tomcat \
 && rm -rf "${CATALINA_BASE}/logs" \
 && mkdir -p /var/log/tomcat \
 && ln -s /var/log/tomcat "${CATALINA_BASE}/logs" \
 && chown -R tomcat:tomcat "${CATALINA_BASE}" /var/log/tomcat

WORKDIR ${CATALINA_BASE}/webapps

VOLUME /cert

EXPOSE 8080/tcp 8443/tcp
