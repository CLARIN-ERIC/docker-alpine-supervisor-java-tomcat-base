#!/bin/sh

catalinaConf="${CATALINA_HOME}/conf/catalina.properties"
if [ "${TOMCAT_CATALINA_CONF}" != "" ]; then
    catalinaConf="${TOMCAT_CATALINA_CONF}"
fi

#Setup keystore if needed
tomcatGenerateKeyStore

#Update tomcat config values (empty ENV variables have no effect)
appendKeyValue "tomcat.util.scan.StandardJarScanFilter.jarsToSkip" "${TOMCAT_JARS_TO_SKIP}" "${catalinaConf}"